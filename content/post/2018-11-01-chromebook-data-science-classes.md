---
title: Chromebook Data Science-Classes
author: Kevin
date: '2018-11-01'
slug: chromebook-data-science-classes
categories:
  - R
tags: ["git"]
header:
  caption: ''
  image: ''
---

I have been working my way through the [Chromebook Data Science](http://jhudatascience.org/chromebookdatascience/index.html) courses. I am enjoying them and I especially love the course on Version Control. I love that they are showing how to manage through the terminal. 

I feel confident enough to try. 
