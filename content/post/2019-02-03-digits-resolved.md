---
title: Digits Resolved
author: Kevin
date: '2019-02-03'
slug: digits-resolved
categories:
  - R
tags:
  - WorkNotes
header:
  caption: ''
  image: ''
---

> 6:00 am-7:00 am

The digit drama was resolved with this line of code.

``r
output$students_served <- renderTable({
   	students_served}, striped = TRUE, digits = 0)
``

It feels strange to put the table argument after the curly brace.


I was able to download the data from qualtrics and update the import script. Ran the process; it seemed to run fine but the counts did not trigger in both the shiny app and status report. 



