---
title: Rules for Tidy Spreadsheets
author: Kevin
date: '2018-11-25'
slug: rules-for-tidy-spreadsheets
categories:
  - R
tags:
  - chromebook data science
  - tidy data
header:
  caption: ''
  image: ''
---

I am a big advocate for teaching tidy data practices to help them manage data. 


Below are some rules for keeping data tidy in a spreadsheet from the **Data Tidying** course and lesson *Tidy Data*


1. Be consistent

2. Choose good names for things

3. Writes dates as YYYY-MM-DD

4. No empty cells

5. Put just one thing in a cell

6. Don't use font or highlighting as a data

7. Save the data as plain text files


Common Problems with Messy Data Sets

1. Column headers are values

2. Multiple variables in a single column

3. Variables have been entered row and columns

4. Multiple types of data in one spreadsheet file

5. A single observation is stored across multiple files