---
title: A Documentation Manifesto
author: Kevin
date: '2019-02-20'
slug: a-documentation-manifesto
categories:
  - R
tags: [Documentation]
header:
  caption: ''
  image: ''
---

Here is an interesting [post](https://enpiar.com/2019/02/17/writing-docs-is-better-than-having-docs/) on writing good documentation. 

> Writing it out helps you to be empathetic with the users.

Maybe, hopefully. 

