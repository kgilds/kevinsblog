---
title: Websites with Rmarkdown
author: Kevin
date: '2018-12-27'
slug: websites-with-rmarkdown
categories:
  - R
tags: []
header:
  caption: ''
  image: ''
---

This morning I wanted to test how easy it would be to render some Rmarkdown files into a website using Gitlbab. I found hosting on Netlify is truly easy and friction less with how you would render the site in Rstudio.

My original goal was to run with hosting with the Gitlab process but I seem to be missing a step. 