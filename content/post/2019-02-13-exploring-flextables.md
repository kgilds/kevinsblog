---
title: Exploring Flextables
author: Kevin
date: '2019-02-13'
slug: exploring-flextables
categories:
  - R
tags:
  - WorkNotes
  - flextable
  - officer
  - janitor
header:
  caption: ''
  image: ''
---

> 5:15 - 6:00 

This morning, I explored using [flextable](https://davidgohel.github.io/flextable/) with [officer](https://davidgohel.github.io/officer/) today. The first issue is that the flextable did not play nice with the janitor function adorn_title. 

The table output is also too wide and was running of the slide. For the webinar, I will create an Rmarkdown document and maybe use Data Table and discuss historic trends. 