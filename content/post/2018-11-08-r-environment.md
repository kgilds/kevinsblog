---
title: R environment
author: Kevin
date: '2018-11-08'
slug: r-environment
categories:
  - R, worknotes
tags: ["Shiny"]
header:
  caption: ''
  image: ''
---

This morning, I re-published a Shiny Dashboard to Shinyapps.io. I was checking the menu items and noted on one page the tables were not rendering.

I checked the rendering on my local machine and everything looked fine.

Back to Shinyapps.io to check the logs. 

I spot the issue--the *select* function not recognized.

> The environment

Dplyr loaded in my environment but I did not load the dplyr library in the app.R file.  

Without the dplyr package loaded R did not know how to process the server side script once it was away from my local machine  and rendered an error message.