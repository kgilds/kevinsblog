---
title: More officer
author: Kevin
date: '2019-02-12'
slug: more-officer
categories:
  - R
tags: 
  - WorkNotes
  - officeR
header:
  caption: ''
  image: ''
---

> 4:30 - 6:00 am

Still working though the flow of creating a PowerPoint though scripts.  This [resource](http://lenkiefer.com/2017/09/23/crafting-a-powerpoint-presentation-with-r/) listed on the package page was both inspiring and helpful. I need to get moving on framework and structure and fill in detail later. 

Sent files with duplicate records to client today; my script to send the e-mail was not working and then I realized the e-mail address was not formatted correctly. :grimacing:



