---
title: My new hammer
author: Kevin
date: '2018-12-12'
slug: my-new-hammer
categories:
  - R
tags:
  - WorkNotes
  - case_when
header:
  caption: ''
  image: ''
---

I replaced another if_else function with the [case_when](https://www.rdocumentation.org/packages/dplyr/versions/0.7.8/topics/case_when) function. 

It is my new :hammer: 