---
title: Tips for generating reports in RMarkdown
author: Kevin
date: '2019-01-20'
slug: tips-for-generating-reports-in-rmarkdown
categories:
  - R
tags:
  - Rmarkdown
header:
  caption: ''
  image: ''
---

This was an interesting [post](https://jozefhajnala.gitlab.io/r/r909-rmarkdown-tips/) I found on Rbloggers today. You can use the infinite_moon_reader function() to live preview a markdown file.



