---
title: Podcast Diversion
author: Kevin
date: '2019-01-03'
slug: podcast-diversion
categories:
  - life
tags:
  - podcast
header:
  caption: ''
  image: ''
---

I found a lot of value listening to this [podcast](<iframe width="560" height="315" src="https://www.youtube.com/embed/b3OjDF0wPPo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>)

It really gives a picture of how the brain works. 
