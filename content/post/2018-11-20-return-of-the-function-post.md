---
title: Return of the Function Post
author: Kevin
date: '2018-11-20'
slug: return-of-the-function-post
categories:
  - R
tags:
  - functions
  - chromebook data science
header:
  caption: ''
  image: ''
---

I knew that a R function returned the last statement evaluated and I never messed with this default--mostly thankful the function worke. 

The example below from the *Writing Functions* Module opened up my imagination to the possibilities. 

```
celsius_to_fahrenheit <- function(C){
F <- C * (9/5) + 32
return(paste("The entered Celsius temperature is", F, "degrees Fahrenheit."))
}
```

