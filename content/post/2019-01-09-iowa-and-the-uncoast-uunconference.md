---
title: Iowa and the Uncoast-Uunconference
author: Kevin
date: '2019-01-09'
slug: iowa-and-the-uncoast-uunconference
categories:
  - R
tags: []
header:
  caption: ''
  image: ''
---

I was excited to see this on twitter <blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">Announcing the <a href="https://twitter.com/hashtag/uncoastUnconf?src=hash&amp;ref_src=twsrc%5Etfw">#uncoastUnconf</a>: an event for <a href="https://twitter.com/hashtag/rstats?src=hash&amp;ref_src=twsrc%5Etfw">#rstats</a> users &amp; developers living in the non-coastal regions  of the USA (AKA flyover country): <a href="https://t.co/aZsvLDFf1E">https://t.co/aZsvLDFf1E</a> <br><br>Apply to attend now! Spots will be limited. <a href="https://t.co/aY4pUW2b6K">pic.twitter.com/aY4pUW2b6K</a></p>&mdash; Dr. Sam Tyner (@sctyner) <a href="https://twitter.com/sctyner/status/1082336062888992768?ref_src=twsrc%5Etfw">January 7, 2019</a></blockquote>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
 I love R and I love Iowa. I applied--I hope you do as well. Here is the [link](http://uuconf.rbind.io/)

