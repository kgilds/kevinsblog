---
title: Still not quite right
author: Kevin
date: '2018-12-11'
slug: still-not-quite-right
categories:
  - R
tags:
  - WorkNotes
header:
  caption: ''
  image: ''
---


I wrote this [post](https://kgilds.rbind.io/post/file-extensions/) yesterday. My theme was correct in that I need to clean up the file name extensions but this was not the reason my app was not rendering correctly. 

It dawned on me that Rstudio will give you a warning if the file extensions are inconsistent. Alas, I have a bug that I am not sure what the problem is.  :slightly_frowning_face:






.


