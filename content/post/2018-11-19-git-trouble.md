---
title: Git Trouble
author: Kevin
date: '2018-11-19'
slug: git-trouble
categories:
  - R
  - git
tags:
  - git
  - error messages
header:
  caption: ''
  image: ''
---

I ran into git trouble Sunday morning by a misguided effort to correct a file conflict during a merge. Not sure what happened there; but I probably should have attempted to resolve from the git-lab interface.

Eventually, I resolved the conflict but could not commit the change, and could not switch branches even when I closed and re-started R-Studio.

I kept getting this error message

```
error: you need to resolve your current index first

```

This [stackoverflow post](https://jhudatascience.org/chromebookdatascience/ bailed me out). 


```
git reset --merge

```