---
title: Shiny Modules Video
author: Kevin
date: '2019-02-05'
slug: shiny-modules-video
categories:
  - R
tags:
  - Shiny
header:
  caption: ''
  image: ''
---

Here is a [link](https://resources.rstudio.com/rstudio-conf-2019/effective-use-of-shiny-modules-in-application-development) to video on Shiny Modules.

This [article](https://shiny.rstudio.com/articles/communicate-bet-modules.html) seems  effective as well.



