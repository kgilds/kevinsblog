---
title: Slow and Deliberate
author: Kevin
date: '2018-12-02'
slug: slow-and-deliberate
categories:
  - R
tags:
  - chromebook data science
  - dpylr
header:
  caption: ''
  image: ''
---

Finished *Tidying Data* section of the **Data Tidying** course today. I finished with a good mark and more importantly a better understanding of the functions and different ways to use familiar dyplr functions along with the pipe.

I remind myself to keep the learning deep and slow.  The next section is strings and that presents more learning opportunities. 
