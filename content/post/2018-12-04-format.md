---
title: format
author: Kevin
date: '2018-12-04'
slug: format
categories:
  - R
tags:
  - chromebook data science
  - FormatC
  - Data Camp
  
header:
  caption: ''
  image: ''
---

[String Manipulation With StringR](https://www.datacamp.com/courses/string-manipulation-in-r-with-stringr) is a course I wish I would have been able to take when I first start working R -especially [formatC](https://www.rdocumentation.org/packages/base/versions/3.5.1/topics/formatC)

I am enjoying the course because, I am being challenged to think through the exercises. 

> However, I am not positive I would have appreciated it as much as I do now. 
