---
title: Summary Stats
author: Kevin
date: '2018-12-26'
slug: summary-stats
categories:
  - R
tags:
  - R
header:
  caption: ''
  image: ''
---

I came across this [post](https://dabblingwithdata.wordpress.com/2018/01/02/my-favourite-r-package-for-summarising-data/) this recently. The examples gave me workflow ideas.  I was interested in learning that you can specify a summary value to run.

Recently, I had extracted some output from [skimr](https://github.com/ropensci/skimr) into a narrative text. I may be able to utilize this capability further. 

