---
title: Drake!
author: Kevin
date: '2019-01-24'
slug: drake
categories:
  - R
tags:
  - workflow
  - WorkNotes
header:
  caption: ''
  image: ''
---

I am curious about using the [Drake package](https://github.com/ropensci/drake) and alter my workflow. However, I need to start small. Here is the [Manuel](https://ropenscilabs.github.io/drake-manual/index.html#the-drake-r-package). I may segment a section because I will need to start slow. 



