---
title: Tables and Knitr
author: Kevin
date: '2019-02-06'
slug: tables-and-knitr
categories:
  - R
tags:
  - WorkNotes
  - Shiny
header:
  caption: ''
  image: ''
---

> 4:30-5:30 

Worked on getting script working and able to render table to reconcile data entry and enrollment. Put in status ran report but when rendering the tables looked funny in both the html and pdf. Make sure `r knitr` is installed. 


I have been thinking about Shiny lately. Here is [link](https://auth0.com/blog/adding-authentication-to-shiny-server/) to an article that may be handy down the road on how to add a log in 
