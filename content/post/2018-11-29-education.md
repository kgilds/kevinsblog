---
title: Education
author: Kevin
date: '2018-11-29'
slug: education
categories:
  - General
tags:
  - podcast
header:
  caption: ''
  image: ''
---

This talk below get me excited riled up.

<iframe src="https://art19.com/shows/akimbo/episodes/130767e0-00fa-464f-8b1e-e21bce1d359d/embed?theme=light-gray-orange" style="width: 100%; height: 200px; border: 0 none;" scrolling="no"></iframe>


My disagreement with Seth's take on memorization is nuanced. I think he is referring to rote memorization and memorization because you have to get an A on a test.  

An interesting book on memory is [Moonwaling with Einstein]
(https://www.amazon.com/Moonwalking-Einstein-Science-Remembering-Everything/dp/0143120530)

A question that comes to mind is the availability of empirical evidence regarding a flipped-classroom that is lectures at night work and problem solving during the day. 
