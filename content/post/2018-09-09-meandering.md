---
title: Submit Buttons
author: Kevin
date: '2018-09-09'
slug: meandering
categories:
  - Issues
tags: ["Qualtrics"]
header:
  caption: ''
  image: ''
---

I had forgotted how to add a "submit" button in Qualtrics. I changed a project around and could not figure how my other "submit"" buttons were rendered. 
I about went crazy trying to figure why my java script was not working. 

It was this easy. 

<iframe width="560" height="315" src="https://www.youtube.com/embed/WpIYct5GwUI" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>


