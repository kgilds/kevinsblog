---
title: Screen To Gif
author: Kevin
date: '2018-12-24'
slug: screen-to-gif
categories:
  - R
tags:
  - blogdown
header:
  caption: ''
  image: ''
---

I came across this open-source [Gif Tool](https://www.screentogif.com/) today from this [blog post](https://www.r-bloggers.com/5-amazing-free-tools-that-can-help-with-publishing-r-results-and-blogging/). I am hoping using this GIF feature can help me spruce up some posts. 

