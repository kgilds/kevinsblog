---
title: file extensions
author: Kevin
date: '2018-12-10'
slug: file-extensions
categories:
  - R
tags:
  - WorkNotes
header:
  caption: ''
  image: ''
---

I have noticed that my inconsistent with the case of file extensions. At times, I am using .rds and other times .Rds.  Some of you realize this is a big deal :grimacing: 

I suspect this is what is wrong with a current application; it runs fine on my local machine but when I render to shinyapps io--it does not render.  

The file extension also make a difference when one is add and committing from the terminal.  

