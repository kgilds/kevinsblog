---
title: This is a good start
author: Kevin
date: '2018-12-14'
slug: this-is-a-good-start
categories:
  - Nonprofits
tags:
  - Metallica
header:
  caption: ''
  image: ''
---

[The All Within My Hands Foundation](https://www.allwithinmyhands.org/news-media/news/metallica-scholars.html) the charitable arm of Metallica announced an initiative to provide ten $100,000 scholarships to students attending community college. 


This seems like a great start and make me proud to be a Metallica fan. 
