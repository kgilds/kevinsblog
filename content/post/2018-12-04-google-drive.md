---
title: 'Google Drive '
author: Kevin
date: '2018-12-04'
slug: google-drive
categories:
  - R
tags:
  - WorkNotes
header:
  caption: ''
  image: ''
---

> worked 2 hours

Implemented import scripts for q1 and then processed/cleaned the data. Status report is ready to go.

Utilized [google drive library](https://googledrive.tidyverse.org/) to send q1 duplicate file to google drive.

I think it would be wise to incorporate into workflow for other files in workflow.


