---
title: Book Website with Rmarkdown
author: Kevin
date: '2018-12-30'
slug: book-website-with-rmarkdown
categories:
  - R
tags:
  - Rmarkdown
header:
  caption: ''
  image: ''
---
My [book website](https://confident-poitras-c71361.netlify.com/index.html) with Rmarkdown files is coming along. Note to self you need to push the CSS and other files in the site_library to Netlify to get the site looking nice. 

