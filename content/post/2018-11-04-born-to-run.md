---
title: Born to Run
author: Kevin
date: '2018-11-04'
slug: born-to-run
categories:
  - books
tags:
  - zen
header:
  caption: ''
  image: ''
---


> The purpose of Zen is the perfection of character


[Zen and the Art of Motorcycle Maintenance](https://www.amazon.com/Zen-Art-Motorcycle-Maintenance-Inquiry/dp/0060589469) was a metaphor for working on the self rather than a bike; much could be said of the book [*Born to Run*](https://www.amazon.com/Born-Run-Hidden-Superathletes-Greatest/dp/0307279189) by Christopher McDougall. 

It was an exciting and interesting read and almost makes me want to run again. 