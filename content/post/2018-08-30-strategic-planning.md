---
title: Strategic Planning
author: Kevin
date: '2018-08-30'
slug: strategic-planning
categories: []
tags: ["strategic-planning", "management", "nonprofit"]
header:
  caption: ''
  image: ''
---

The case study from Berger and Livingston (2011) depicts the culture at St. Martin de Porres High School (SMdP) as place where teachers and administrators wanted success for students but did not consistently implement action to help students develop their full potential. The assistant principal noted that the culture of the school felt like a junior high rather than a college preparatory school. The article details the chaos of students banging on windows and doors after the main doors had been locked.  


Many teachers seem to have lost motivation and were resistant to their own evaluation and accountability. For instance, the article reports that teachers were providing answers to online test and putting all the blame on the students for poor performance. Additional problems noted in the article include a belief that school policies were not implemented in a consistent manner or that the school even had a philosophy of learning. For example, there was an inadequate system for monitoring student credit recovery. Investigation indicated that many seniors were missing credits from their freshman year and this contributed to a delay in graduation for students. 


There was a misalignment between the mission of the school and the culture of the school. The school was established as a college preparatory school that established standards for students to be prepared for work, prepared for college, and committed to justice and peace. However at last check, a quarter of students did not graduate on time and students had 10 tardies on average. The performance of the schools did not support that this was a college preparatory school, and the teachers and students did not believe the school was achieving academic excellence.    

The biggest challenge for the principal and assistant principal is to align the culture of the school with the mission statement of the school.Winston (2008) points out that effective organizations have goal clarity, supportive systems, and performance enhancing feedback systems.The administration has identified critical gaps but will need to develop systems to develop goal clarity, feedback mechanisms, and build the leadership capacity of both the teachers and students to realize the mission and standards of the school.  

The administration can utilize the mission and the three standards to align their strategic priorities--specific actions that need to occur in order to reach the standards of being prepared for work, prepared for college, and commitment to justice and peace (Kresege Foundation, 2011). The principal and assistant principal will not be able to implement a plan and achieve success without the help and buy in from teachers, students and parents. The new administration will need to develop the leadership of the teachers in order to role model leadership behavior to students. 

The article from Winston (2008) notes that the most effective organizations view leadership development as an investment. Winston (2008) notes that effective leaders, "...develop subordinates confidence in their ability to lead, manage, and impact business results" (p.9). Currently, it seems the teachers do not feel empowered to lead and manage the students.

A way to get buy in from the teachers is to include them in the planning process.Winston (2008) discussed leadership practice of teams discussing the state-of-the-practice and the state-of-the art in the world. The school belongs to the [Cristo Rey Network ](https://www.cristoreynetwork.org/)--SMdP may be able to get technical assistance from the network of schools who may have overcome similar obstacles. 


The administration should also work to involve students and parents in strategic planning as they are key stakeholders. The case study does not indicate if SMdP has an advisory committee or parent teacher association. If not, this is an avenue for development to foster involvement. An idea to get the students involved is to work with them to develop a visual logic model to help them understand the connections between the strategic priorities and the intended impact--make it clear to the students why it matters. 

The key takeaway is that SMdP did not have a plan to realize their mission and standards. However, a plan without an emphasis on leadership development may not be effective. In summary it is critical for an organization to have both an effective plan and people to execute the plan.  




### References: 


Berger, Gail & Livingston, Howard  (2010). *Creating a Culture of Empowerment and Accountability at St. Martin de Porres High School A*. Kellog School of Management
 
Kresge Foundation  (2011). *A Guide to Strategic Planning*
 
Winston, Michael (2008). *Leadership Development Through Thick and Thin*
 
 