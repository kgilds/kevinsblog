---
title: Styling Rmarkdown Reports
author: Kevin
date: '2019-03-04'
slug: styling-rmarkdown-reports
categories:
  - R
tags: [rmdformats]
header:
  caption: ''
  image: ''
---

I have been looking for this [package](https://github.com/juba/rmdformats) my whole life. 

I am excited to use the material design theme; this is just what I had in mind. 
