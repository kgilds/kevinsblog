---
title: UseR!Tampa
author: Kevin
date: '2018-12-18'
slug: user-tampa
categories:
  - R
tags: [UseR!Tampa]
header:
  caption: ''
  image: ''
---

Tonight is a meeting of [UseR!Tampa](https://tampausers.github.io/)

We meet at 

Southern Brewing & Winemaking
4500 North Nebraska Avenue, Tampa

You should check it out. :grin:


