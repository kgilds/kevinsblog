---
title: Design Model of a Data Analysis
author: Kevin
date: '2018-12-07'
slug: design-model-of-a-data-analysis
categories:
  - Data science
tags: []
header:
  caption: ''
  image: ''
---


I am not sure how I missed this [post](https://simplystatistics.org/2018/09/14/divergent-and-convergent-phases-of-data-analysis/) by Roger Peng.  I was blown away by it.


