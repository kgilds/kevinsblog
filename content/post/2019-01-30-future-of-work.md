---
title: Future of Work
author: Kevin
date: '2019-01-30'
slug: future-of-work
categories:
  - podcast
tags:
  - podcast
header:
  caption: ''
  image: ''
---


This [episode](https://megaphone.link/VMP3578952236) of Recode/Decode discussed the future of work with Ellen Shell. She is right it; the future requires more than just learning to code. They had a interesting discussion of the history of the job and prior transitions of the nature of work. It made me think of the book [*White Collar*](https://en.wikipedia.org/wiki/White_Collar:_The_American_Middle_Classes).

I found myself in disagreement with her that most everything will be automated; my thinking is that it will be humans working with machines--however I could just be merely hopeful. 
