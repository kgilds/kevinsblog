---
title: Advice Columns
author: Kevin
date: '2018-11-02'
slug: advice-columns
categories:
  - General
tags: ["podcast", "science"]
header:
  caption: ''
  image: ''
---

This podcast intrigued me the other day. Advice based on science from Amy Alkon. 

<iframe width="560" height="315" src="https://www.youtube.com/embed/OF0_L_U2pFE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

I was not as impressed with the [blog layout](http://www.advicegoddess.com/goddessblog.html) but the content seems to be a different but reasoned arguments.


