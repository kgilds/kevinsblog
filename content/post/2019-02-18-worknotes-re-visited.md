---
title: WorkNotes Re-visited
author: Kevin
date: '2019-02-18'
slug: worknotes-re-visited
categories:
  - R
tags:
  - WorkNotes
  - Chromegook Data Sciene
header:
  caption: ''
  image: ''
---

I was scanning the Writing and Oral Communication Course in the Chromebook Data Science Series. The module *How to Keep a Data Science Notebook* caught my attention and the suggestion of using a tool called benchling. I have been putting my worknotes on the blog; although benefical for me to write for the reader it may be off putting because there is no context. 

I will try benchling and just link to my notes from here if anyone is interested. I would like for my blog post to be more informative. 

Here is my first  [WorkNote](https://benchling.com/s/etr-aNArT8uq8FKPwSpNQy2L) 