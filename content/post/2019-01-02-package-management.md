---
title: Package Management
author: Kevin
date: '2019-01-02'
slug: package-management
categories:
  - R
tags:
  - WorkNotes
header:
  caption: ''
  image: ''
---

>  5:00 am - 7:00 am

This morning worked on updating data from 17-18 into the getReal2 package. This should have been done last summer! It has been a pain because I have had to wrangle the data to fit the structure. However, with attendance data; I modified the structure a bit--I added a maintenance variable to fit the outcome nuance. 
