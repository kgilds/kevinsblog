---
title: Substr as function of the day
author: Kevin
date: '2018-11-19'
slug: substr-as-function-of-the-day
categories:
  - R
  - worknotes
tags:
  - WorkNotes
header:
  caption: ''
  image: ''
---

To solve my conditional statement issues from this [post](https://kgilds.rbind.io/post/case-when/); I wound up creating two more columns. 

The *case_when* seems to need an exact conditional statement--not this *"^313"*  I wound up extracting the first three elements of the student_code vector with the *substr function*.



The entire script is below




```
council_code <- substr(student$student_code, 1,3) #this takes the element 1-3

student$council_code <- council_code  #put the council_code vector back into the main data frame to use with case_when


student <- student %>%
mutate(council_count = case_when(
	council_code == "321" ~ "Southeast Council",
	council_code == "313" ~ "Gateway Council",
	council_code == "320" ~ "West Central Council",
	council_code == "312" ~ "Citrus Council",
	TRUE ~ "Unassigned"
))

```