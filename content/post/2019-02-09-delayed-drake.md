---
title: Delayed Drake
author: Kevin
date: '2019-02-09'
slug: delayed-drake
categories:
  - R
tags:
  - WorkNotes
  - Drake  
  - Chromegook Data Sciene
header:
  caption: ''
  image: ''
---

> 4:15 -5:30 am

This morning I committed and pushed changes for getREAL2 package. I worked on and sent status report. 

I installed and attempted to use the Drake package and that failed quickly-but will try again.

I worked on the Chrombebook Data science on Inference. I understand confounders in theory but not in practice. 