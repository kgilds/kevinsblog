---
title: Notes on good plots
author: Kevin
date: '2018-12-20'
slug: notes-on-good-plots
categories:
  - R
tags:
  - chromebook data science
header:
  caption: ''
  image: ''
---

I am currently working through the *Data Visualization* course from Chromebook Data Science.



I am working to incorporate the General Feature of Plots into my practice.

1. The axes labels are clear.

2. The text on the graph is large enough to see

3. Non misleading axes

4. You use the best graph for the type of data you are displaying

This morning, I spent some time fixing up default axes labels. I am guilty of using defaults. 