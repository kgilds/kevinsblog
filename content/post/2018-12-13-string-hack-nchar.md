---
title: String Hack (nchar)
author: Kevin
date: '2018-12-13'
slug: string-hack-nchar
categories:
  - R
tags:
  - WorkNotes
  - substr
header:
  caption: ''
  image: ''
---

> 4:50-6:00 am

I was working to figure out how to subset a string with vary lengths. I came across this [post](https://discuss.analyticsvidhya.com/t/how-to-use-substr-function-in-r-to-extract-a-specific-part-of-a-character-string/1590). 


Where substr is the function.

The first argument is the string character, 3 is the starting position and nchar gives you the ending position. 

```r
library(stringr)
substr(string1, 3, nchar(string1))

```





