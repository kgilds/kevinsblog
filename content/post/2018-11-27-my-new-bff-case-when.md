---
title: My New BFF--case_when
author: Kevin
date: '2018-11-27'
slug: my-new-bff-case-when
categories:
  - R
tags:
  - dplyr
header:
  caption: ''
  image: ''
---

[case_when](https://www.rdocumentation.org/packages/dplyr/versions/0.7.8/topics/case_when) is my current BFF

I was having trouble repacling values based on a condition. I needed the transform function to work with case_when. 

This [stackoverflow post](https://stackoverflow.com/questions/8214303/conditional-replacement-of-values-in-a-data-frame) provided the solution to two pressing issues in my workflow. 


Below is code from the post. 

```
transform(df, est = case_when(
    b == 0 ~ (a - 5)/2.53, 
    TRUE   ~ est 
))

```


