---
title: Towards understanding ggplot2
author: Kevin
date: '2018-12-22'
slug: towards-understanding-ggplot2
categories:
  - R
tags:
  - chromebook data science
  - WorkNotes
header:
  caption: ''
  image: ''
---

This morning I was working on a bar plot and I figured out a way to display the information in a different way. I switched the x axis from being success to council and filling by success.  I wish I could share the plot.

The Data Visualization course from the Chromebook Data Science series has been great and I give credit to the course. I am finally learning how to customize my plots. I never understood themes and code required to increase the text on the axis.  I am stoked about this new knowledge :grin: 