---
title: Start-up Nonprofits
author: Kevin
date: '2018-11-15'
slug: start-up-nonprofits
categories:
  - Nonprofits
tags:
  - Data Science
header:
  caption: ''
  image: ''
---

I listened to this podcast this morning.

<iframe src="https://art19.com/shows/recode-decode/episodes/d2d5c7d1-fbd1-411c-8120-ee81b480a87c/embed?theme=dark-custom" style="width: 100%; height: 200px; border: 0 none;" scrolling="no"></iframe>


I understand why the conversation stayed at a high level and I thought some good insights were shared; I wish it could have gotten more into the weeds. 

