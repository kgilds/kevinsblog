---
title: Change Remote
author: Kevin
date: '2018-09-14'
slug: change-remote
categories:
  - Git
tags: ["Git"]
header:
  caption: ''
  image: ''
---

I determined to change git repositories recently. It was easy but a somewhat tricky part was changing the communication channel between my local machine and the new repository.

```
git remote set-url origin (link)

```

Github has a helpful [tutorial](https://help.github.com/articles/changing-a-remote-s-url/) on the process 