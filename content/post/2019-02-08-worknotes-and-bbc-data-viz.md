---
title: Worknotes and BBC Data Viz
author: Kevin
date: '2019-02-08'
slug: worknotes-and-bbc-data-viz
categories:
  - R
tags:
  - WorkNotes
header:
  caption: ''
  image: ''
---

> 6:12 am  - 6:30 am

And I overslept this morning. I managed to get in my R time by adding additional pre range functions in the Get Real package. Need to make sure I commit and push these changes.

I found this [R-Cookbook](https://bbc.github.io/rcookbook/) from the BBC. The Medium [post](https://medium.com/bbc-visual-and-data-journalism/how-the-bbc-visual-and-data-journalism-team-works-with-graphics-in-r-ed0b35693535) about the scripting workflow is great as well. 