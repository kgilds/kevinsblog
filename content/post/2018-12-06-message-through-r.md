---
title: Message through R
author: Kevin
date: '2018-12-06'
slug: message-through-r
categories:
  - R
tags:
  - WorkNotes
  - gmailr
header:
  caption: ''
  image: ''
---

I started using [gmailr](https://github.com/jimhester/gmailr) last year but could never figure out how to send an attachment.

I was able to finally figure out how to send the attachment but then the content of the body would not send.


[This issue](https://github.com/jimhester/gmailr/issues/81) in the Github Repository helped me figure it out partially. Now, I can send both the body and attachment but my only problem is that it sends twice. 

