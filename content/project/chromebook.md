+++
# Date this page was created.
date = 2018-10-01T00:00:00

# Project title.
title = "Chromebook Data Science"

# Project summary to display on homepage.
summary = "Kevin's progress through the Chromebook Data Science Course"

# Optional image to display on homepage (relative to `static/img/` folder).
image_preview = "chromebookk.jpg"

# Tags: can be used for filtering projects.
# Example: `tags = ["machine-learning", "deep-learning"]`
tags = ["Chromebook Data Science"]

# Optional external URL for project (replaces project detail page).
#external_link = 

# Does the project detail page use math formatting?
math = false

url_pdf = "https://drive.google.com/file/d/1rd0sYH9yIUhSSANvVHkjKqpPl5lgQDqY/"
+++


## Home page to the project:
[Chromebook Book Data Science](http://jhudatascience.org/chromebookdatascience/index.html) 

Chromebook Data Science is a Massive Online Course to help anyone learn data science with only a chromebook. 

## Blog Posts

[Learning Posts](https://kgilds.rbind.io/tags/chromebook-data-science/)




## Results

0. [Introduction to Chromebook Data Science](https://drive.google.com/file/d/1rd0sYH9yIUhSSANvVHkjKqpPl5lgQDqY/view?usp=sharing)

1. [How to use a Chromebook](https://drive.google.com/file/d/1gW0ptvvhQPtmARnEBWl2DHPjRUrg8AMQ/view?usp=sharing)

2. [Google and the Cloud](https://drive.google.com/file/d/1SIQUNa_b37VA_HFG0iuX9MTsP2zd0jCF/view?usp=sharing)

3. [Organizing Data Science Projects](https://drive.google.com/file/d/1qy38DLzwtZ0eypWiZt6YGAqrpWMY9AIr/view?usp=sharing)

4. [Version Control](https://drive.google.com/file/d/1o7rQRuEydmAukUvriTXLR3oJZIixfaKy/view?usp=sharing)

5. [Introduction to R](https://drive.google.com/file/d/1-lS0M7-Uc4YHRU2vUpUoaSYky5jvFxac/view?usp=sharing)

6. [Data Tidying](https://drive.google.com/file/d/12kM3QtOk9qloZFvB39O9pXawPMuubsKn/view?usp=sharing)

7. [Data Visualization](https://drive.google.com/file/d/1kQKtEsKJza0xKm9qEdBLPdbTARhK6L0I/view?usp=sharing)

8. [Getting Data](https://drive.google.com/file/d/1d9B2xkHsQ0o5ipLIRv7YWOPifbrdsNn7/view?usp=sharing)