+++
# Date this page was created.
date = 2018-09-08T00:00:00

# Project title.
title = "Mapping Florida Absences"

# Project summary to display on homepage.
summary = "Mapping Florida Absences based on Florida Deparment of Education Data"

# Optional image to display on homepage (relative to `static/img/` folder).
image_preview = "mapped.png"

# Tags: can be used for filtering projects.
# Example: `tags = ["machine-learning", "deep-learning"]`
tags = ["Education Data"]

# Optional external URL for project (replaces project detail page).
external_link = "https://kgilds.rbind.io/post/mapping-florida/"

# Does the project detail page use math formatting?
math = false

+++

