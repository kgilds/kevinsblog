+++
# Date this page was created.
date = 2016-04-27T00:00:00

# Project title.
title = "Freeing Educational Data from the Florida Department of Education"

# Project summary to display on homepage.
summary = "Florida Absences"

# Optional image to display on homepage (relative to `static/img/` folder).
image_preview = "boards.jpg"

# Tags: can be used for filtering projects.
# Example: `tags = ["machine-learning", "deep-learning"]`
tags = ["Education Data"]

# Optional external URL for project (replaces project detail page).
external_link = "https://tidydatabykwg57.shinyapps.io/flabsences/"

# Does the project detail page use math formatting?
math = false

+++

