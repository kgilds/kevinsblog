+++
# About/Biography widget.
widget = "about"
active = true
date = 2016-04-20T00:00:00

# Order that this section will appear in.
weight = 5

# List your academic interests.
[interests]
  interests = [
    "Education Data",
    "Self-Directed Care",
    "Youth Aging Out of Foster Care"
  ]

# List your qualifications (such as academic degrees).
[[education.courses]]
  course = "Masters of Public Administration"
  institution = "University of South Florida"
  

[[education.courses]]
  course = "BA Sociology"
  institution = "University of Northern Iowa"
  
[[education.courses]]
  course = "AA Paralegal/Legal Assistant"
  institution = "Kirkwood Community College"
  
+++

# Biography

Kevin Gilds is a nonprofit professional located in Lakeland Florida--between Tampa and Orlando. I came to *R* from a unique path--the social service sector and  learning *R* and basic data science has helped me wrangle nonprofit program data and develop methods to make data processing and reporting easier.  



