+++
# Feature/Skill widget.
widget = "featurette"
active = true
date = "2017-11-21"

# Order that this section will appear in.
weight = 20

# Add/remove as many `[[feature]]` blocks below as you like.
# See `config.toml` for more info on available icons.


[[feature]]
  icon = "r-project"
  icon_pack = "fab-fa"
  name = "R"
  description = ""
  
[[feature]]
  icon = "chart-line"
  icon_pack = "fas"
  name = "Data Visualization"
  description = ""  
  
[[feature]]
  icon = "table"
  icon_pack = "fas"
  name = "Data Wrangling"
  description = ""


[[feature]]
  icon = "files-o"
  icon_pack = "fa"
  name = "Data Driven"
  description = "Expertise in Collecting, Managing, and Reporting Nonprofit Data"
  
[[feature]]
  icon = "heart"
  icon_pack = "fa"
  name = "Nonprofit Organizations"
  description = "10 + years in the nonprofit sector"
  
  [[feature]]
  icon = "github" 
  icon_pack = "fa"
  name = "Git"
  
  [[feature]]
  icon = "database" 
  icon_pack = "fa"
  name = "SQL"



+++
